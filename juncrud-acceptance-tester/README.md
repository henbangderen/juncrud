# juncrud acceptance tester

UI testing

## Prerequisites
Checked on 
* nodejs v10.19.0
* npm 6.14.4

## Overview

Tests using the puppeteer 

## Launch
```
	npm install
	export PUPPETEER_EXECUTABLE_PATH=HERE_PATH_TO_THE_CHROMIUM_BROWSER
	npm test
```
	Check the html report in the 'target' folder
    

##  License
Copyright © 2020 Roman Levakin
Distributed under the Eclipse Public License 1.0
