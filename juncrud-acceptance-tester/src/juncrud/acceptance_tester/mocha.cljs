(ns juncrud.acceptance-tester.mocha
  (:require ["puppeteer-core" :as puppeteer]
            ["mochawesome/addContext" :as addContext]
            [cljs.test :refer-macros [deftest is testing]]
            [cljs.core.async :as a]
            [cljs.core.async :refer-macros [go]]
            [cljs.core.async.interop :refer-macros [<p!]]
            [promesa.core :as p]
            [clojure.string :as s])
  (:require-macros [latte.core :refer [describe it]]))

(def chrome-executable-path "/media/playful/SeagateExpansionDrive/KODE/clojurescript/shadow-cljs/chrome-linux/chrome")
(def url "http://localhost:9090")
(def root "/patients#/patients")

(defn clear-element [page id]
  (p/let [id id]
   (.evaluate page #((let [v (.getElementById js/document id)]
                       (set! (.-value v) ""))))))

(defn create-patient [page x]
  (p/let []
    (.waitForSelector page ".adder")
    (.click page ".adder")
    (.waitForSelector page ".addform > input")
    (.type page "input#last-name", (:last-name x))
    (.type page "#first-name", (:first-name x))

    (.evaluate page #((let [v (.getElementById js/document "birthdate")]
                       (set! (.-value v) ""))))
    (.type page "#birthdate" (:birth-date x))

    (.select page "#gender" (:gender x))

    (clear-element page "address")
    (.type page "#address" (:address x))
    
    (.type page "#ensuarance-id" (:ensuarance-id x))
    (.click page "html body div#app div div.container form.addform input")
    (.waitForNavigation page)
    ))

(defn get-page[]
  (p/let [browser
          (.launch puppeteer
                   #js{:headless false
                       :slowMo 100
                       :executablePath chrome-executable-path
                       })
          page (.newPage browser)]
    page))

(describe
   "Positive cases"
   :timeout 30000
   (this-as this
     (it "Create patient" [done]
         (p/let [page (get-page)]
           (.goto page (str url root))
           (create-patient page {:first-name "Eva"
                                 :last-name "Luator"
                                 :birth-date "1998-01-09"
                                 :gender "F"
                                 :address "Red Square 1"
                                 :ensuarance-id "123456"})
           ;(done)
           )))

   ;; (it "Delete patient" [done]
   ;;     (p/let [page (get-page)]
   ;;       (.goto page (str url root))
   ;;       (create-patient page {:first-name "Tom"
   ;;                             :last-name "Smith"
   ;;                             :second-name "Jr"})
   ;;       (done)
   ;;         ))
   )
