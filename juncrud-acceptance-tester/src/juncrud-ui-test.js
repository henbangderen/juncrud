const addContext = require('mochawesome/addContext');
const puppeteer = require('puppeteer-core')
const assert = require('assert').strict

const chromeExecutablePath = process.env.PUPPETEER_EXECUTABLE_PATH
const url = "http://localhost:9500"
const root = "/patients#/patients" 

async function clear(page, selector) {
    await page.evaluate(selector => {
        document.querySelector(selector).value = "";
    }, selector);
}

async function type(page, selector, value){
	 await clear(page,selector)
	 await page.type(selector, value)
}

async function createPatient(page, o){
	 await page.waitForSelector(".adder")
	 await page.click(".adder")
	 await page.waitForSelector(".addform > input")
	 await type(page, "input#last-name", o.lastName)
    await type(page, "input#first-name", o.firstName)
	 o.secondName && await type(page, "input#sname", o.secondName)
	 await type(page, "input#birthdate", o.birthDate)
	 await type(page, "input#address", o.address)
	 await type(page, "input#ensuarance-id", o.ensuaranceId)

	 const createUrl = url + '/patients/create'
	 console.log("expected: " +  createUrl)
	 const [createResponse] = await Promise.all([
		  page.waitForResponse(
		  response => {
				console.log("responce: " +  response.url())
				if (response.url() === createUrl
					 && response.status() === 200){
					 return response
				}
		  }),
		  page.click(".submitter")]);
    await page.waitForSelector(".submitter")

	 const id = await createResponse.text();
	 console.log("created with id '" + id + "'")
    return id
}

async function editPatient(page, id, o){
	 await page.waitForSelector("button.editor")
	 await page.click("button.editor")	 
    await page.waitForSelector("form.editForm")
	 if (o.lastName){
		  await type(page, "input#last-name", o.lastName)
	 }

	 if (o.firstName){
		  await type(page, "input#first-name", o.firstName)
	 }
    
	 if (o.secondName){
		  await type(page, "input#sname", o.secondName)
	 }

	 if (o.birthDate){
		  await type(page, "input#birthdate", o.birthDate)
	 }

	 if (o.address){
		  await type(page, "input#address", o.address)
	 }

	 if (o.ensuaranceId){
		  await type(page, "input#ensuarance-id", o.ensuaranceId)
	 }

	 const expectedUrl = url + '/patients/modify'
    console.log("expected: " +  expectedUrl)
	 const [response] = await Promise.all([
		  page.waitForResponse(
		  response => {
				console.log("responce: " +  response.url())
				if (response.url() === expectedUrl
					 && response.status() === 200){
					 return response
				}
		  }),
		  page.click("form.editForm input[type='submit']")]);
	 
    await page.waitForSelector(".recently-modified")
	 const responseId = await response.text();
	 console.log("modified id: '" + responseId + "'")
	 assert.equal(id, responseId)
	 return id
}

async function findPatientBySearch(page, searchStr){
    await type(page, "input#search", searchStr)
	 await page.click("button#go")
}

async function selectPatientById(page, id){
	 const element = await page.$('tr[id="' + id + '"]')
	 console.log("found:  '" + element.tagName + "'")
	 element.click()
}

describe('js suite', function () {
	 let browser
	 let page
	 this.timeout(30000)

	 before(async () => {
		  browser = await puppeteer.launch({headless: false,
														// slowMo: 50,
														executablePath: chromeExecutablePath});
		  page = await browser.newPage();
	 })
	 
	 after(async () => {
		  //await browser.close()
	 })

	 it('create patient', async function () {
        await page.goto(url + root)
		  await createPatient(page, {lastName: "Luator",
											  firstName: "Eva",
											  birthDate: "04111929",
											  address: "Bryton beach",
											  ensuaranceId: "1234123412341236"
											 })
        await page.click("a[href='#/patients']")
		  await page.waitForSelector(".adder")
	 })	 

	 it('edit patient', async function(){
        await page.goto(url + root)
		  const searchString = "C1lDs 5K65075k3jx"
		  const id = await createPatient(page, {lastName: "C1lDs",
															 firstName: "5K65075k3jx",
															 secondName: "editPatientTest",
															 birthDate: "03111989",
															 address: "",
															 ensuaranceId: "1234123412341234"
															})
		  await page.click("a[href='#/patients']")
		  await page.waitForSelector("button.editor")
		  await findPatientBySearch(page, searchString)
		  await page.waitForSelector('tr[id="' + id + '"]')
		  await selectPatientById(page, id)
        await editPatient(page, id, {address: "Red Square, 1"})
    })
	 
	 it('delete patient', async function () {
        await page.goto(url + root)
		  await page.on('dialog', async function(dialog){
				console.log("dialog: " + dialog.message())
				await dialog.accept();
		  })
		  
		  const searchString = "Bryton beach, 22, 222"
		  const id = await createPatient(page, {lastName: "zzzWill",
															 firstName: "Be",
															 secondName: "Deleted",
															 birthDate: "04111929",
															 address: searchString,
															 ensuaranceId: "1234123412341235"
															})
		  await page.click("a[href='#/patients']")
		  await page.waitForSelector(".adder")
		  await findPatientBySearch(page, searchString)
		  await page.waitForSelector('tr[id="' + id + '"]')
		  await selectPatientById(page, id)
		  await page.click("button.remover")
		  await page.waitForSelector("button.remover")
	 })
});
