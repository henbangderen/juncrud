(ns backend.juncrud.crud-util-test
  (:require
   [backend.juncrud.crud-util :as util]
   [clojure.pprint :as pp])
  (:use [clojure.test]))

(defn mock-id-generator [] 777)

(def patient
  {:name "CP6e1NRD1gvCeqxkn0f4xEUQ",
   :gender "F",
   :birthdate "1983-10-19",
   :address "KtQzSkgjt1OU6Z",
   :ensuarance-id "9roL3AxPE2NkzXZ0OoKd6aqp1317GH",
   :id 1})

;; Caveat: ID is 777 it is normal. It does not matter what id comes from UI
;; Correct ID is generated on DB
(defn patient-from-db[]
  (merge patient {:id 777}))

(def entity
  {:things {:id 777,
            :name "CP6e1NRD1gvCeqxkn0f4xEUQ",
            :type "patient"},
   :params [[:id :name :value :thing_id]
            [777 "gender" "F" 777]
            [777 "birthdate" "1983-10-19" 777]
            [777 "address" "KtQzSkgjt1OU6Z" 777]
            [777 "ensuarance-id" "9roL3AxPE2NkzXZ0OoKd6aqp1317GH" 777]]})

(deftest patient->entity-test
  (is (= entity
         (util/patient->entity patient mock-id-generator))))

(deftest entity->patient-test
  (is (= (patient-from-db)
         (util/entity->patient entity))))
