(ns backend.juncrud.server-test
  (:require [backend.juncrud.crud :as crud]
            [backend.juncrud.server :as server]
            [clojure.java.shell :refer [sh]]
            [org.httpkit.client :as http]
            [clojure.test :as t]
            ))

(crud/set-db! (assoc crud/db :port "6432"))
(def yamls-dir "devops")
(def container-name "juncrud-testdb")
(sh "docker-compose" "down" "--volumes" "--remove-orphans" :dir yamls-dir)
;docker-compose -f docker-compose-test-db.yaml up --detach
(sh "docker-compose"
    "-f"
    "docker-compose-test-db.yaml"
    "up"
    "--force-recreate"
    "--detach"
    :dir yamls-dir)

(server/start-server)
(def url "http://localhost:9090")

(let [response1 (http/get (str url "/count-patients"))]
  ;; Other keys :headers :body :error :opts
  (println "response1's status: " (:status @response1))
  (println @response1))

(sh "docker" "container" "stop" container-name)








