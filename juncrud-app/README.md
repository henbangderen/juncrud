# juncrud-app

Sample clojurescript (reagent) + postgres CRUD app. 
View, add, remove and edit a patient info.

## Prerequisites

* jdk >= 1.8
* docker >= 19.03.13
* kubectl >= 1.19.3 (optional)

## Development
To get an interactive development environment:

* start a test database
``` 
docker-compose -f devops/docker-compose.yaml up -d 
```	
* start clojure/clojurescript REPLs in Emacs:
  cider-jack-in-clj&cljs
  
  choose figwheel-main opens a browser on http://localhost:9500/patients#/patients automatically

* to stop docker containers:
``` 
docker container stop devops_juncruddb_1 devops_pgadmin_1 
```
  
## Build
```
lein do clean, fig:compile, test, uberjar 
```

## Run
* run the uberjar from the 'target' dir
```
java -jar jun-crud-app*-standalone.jar

```

* run with docker
```
docker build --tag juncrudapp -f devops/Dockerfile target
docker run --name juncrud-app-container --env JUNCRUD_DB_HOST="0.0.0.0" -p 3000:9090 --rm junapp
```

* run in minikube
```
minikube start; eval $(minikube docker-env);
devops/kubernetes/apply.sh
```

## TODO
* rewrite a sql template to use bind variables
* refactor 

##  License
Copyright © 2020 Roman Levakin
Distributed under the Eclipse Public License 1.0
