(defproject jun-crud-app "0.1.0-SNAPSHOT"
  :description "Create read update delete web application storing data in postgres"
  :min-lein-version "2.7.1"

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.773"]

                 ;backend
                 [http-kit "2.3.0"]
                 [compojure "1.6.2"]
                 [ring/ring-defaults "0.3.2"]
                 [com.taoensso/timbre "5.1.0"]
                 [seancorfield/next.jdbc "1.1.588"]
                 [org.postgresql/postgresql "42.2.18.jre7"]
                 [org.clojure/test.check "1.1.0"]
                 [org.clojure/spec.alpha "0.2.187"]
                 [org.clojure/data.json "1.0.0"]
                 [com.outpace/config "0.13.4"]
                 
                 ;frontend
                 [reagent "0.10.0" ]
                 [reagent-forms "0.5.44"]
                 [cider/piggieback "0.4.2"];for repl
                 [cljs-ajax "0.8.1"]
                 [org.clojure/core.async "1.3.610"]
                 [metosin/reitit "0.5.5"]
                 [metosin/reitit-spec "0.5.5"]
                 [metosin/reitit-frontend "0.5.5"]
                 [fipp "0.6.23"]]
  :source-paths ["src"]
  
  :aliases {"fig"       ["trampoline" "run" "-m" "figwheel.main"]
            "fig:build" ["trampoline" "run" "-m" "figwheel.main" "--build" "dev" "--repl"]
            "fig:min"   ["run" "-m" "figwheel.main" "-O" "advanced" "-bo" "dev"]
            "fig:compile" ["run" "-m" "figwheel.main" "--build-once" "dev"]
            "start-server" ["trampoline" "run" "-m" "backend.juncrud.server"]
            "config" ["run" "-m" "outpace.config.generate"]
            }

  :main backend.juncrud.server
  :aot [backend.juncrud.server]
  
  :profiles {:dev {:dependencies [[com.bhauman/figwheel-main "0.2.11"]
                                  [com.bhauman/rebel-readline-cljs "0.1.4"]]
                   
                   :resource-paths ["target"]
                   ;; need to add the compiled assets to the :clean-targets
                   :clean-targets ^{:protect false} ["target"]
                   }}

  :repl-options {:nrepl-middleware [cider.piggieback/wrap-cljs-repl]
                 ;:port 4001
                 }
  )

