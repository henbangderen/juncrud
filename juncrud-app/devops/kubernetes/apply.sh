#/bin/sh

eval $(minikube docker-env)
docker build --tag juncrudapp -f ../Dockerfile ../../target

kubectl delete service junpostgres-service
kubectl delete deployment junpostgres-deployment 
kubectl delete persistentvolumes juncrud-persistance-volume
kubectl delete secret junpostgres-secret
kubectl delete persistentvolumeclaims juncrud-persistent-volume-claim
kubectl delete configmap junpostgres-configmap
kubectl delete service juncrud-webapp-service
kubectl delete deployment juncrud-webapp
kubectl delete configmap init-database-configmap

### kubectl apply commands in order
kubectl apply -f postgres-secret.yaml
kubectl apply -f persistent-volume.yaml
kubectl apply -f postgres.yaml
kubectl apply -f postgres-configmap.yaml 
kubectl apply -f web-app.yaml

