### prepare docker-env
eval $(minikube docker-env)
docker build --tag juncrudapp -f devops/Dockerfile target

#kubectl apply -f app.yaml
#kubectl exec -it juncrud-app-pod -c sidecar -- /bin/sh

### secret must be base64 encoded
	 echo -n 'admin' | base64
	 echo -n 'secret' | base64

### kubectl apply commands in order
    
    kubectl apply -f postgres-secret.yaml
	 kubectl apply -f persistent-volume.yaml
    kubectl apply -f postgres.yaml
    kubectl apply -f postgres-configmap.yaml 
    kubectl apply -f web-app.yaml

### kubectl get commands

    kubectl get pod
    kubectl get pod --watch
    kubectl get pod -o wide
    kubectl get service
    kubectl get secret
    kubectl get all | grep mongodb

### kubectl debugging commands

    kubectl describe pod mongodb-deployment-xxxxxx
    kubectl describe service mongodb-service
    kubectl logs mongo-express-xxxxxx

### give a URL to external service in minikube

    minikube service mongo-express-service


  