#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	  DROP DATABASE if exists jundb;
	  DROP ROLE if exists jun;

	  CREATE ROLE jun LOGIN PASSWORD 'jun' CREATEDB CREATEROLE;
	  CREATE DATABASE jundb OWNER jun;
EOSQL
