(ns common.juncrud.patient-spec
    (:require [clojure.spec.alpha :as s]
            [clojure.test.check.generators :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.pprint :as pp]))

(s/def ::name (s/and string? not-empty))
#_(s/conform ::name "Ivanov")
(s/def ::gender #{"M" "F"})

(s/def ::birthdate
  (s/with-gen
    (fn [x] (re-matches #"\d\d\d\d-\d\d-\d\d" x))
    #(gen/fmap
      (fn[_] (str "19" (rand-int 10) (rand-int 10)
                  (.format (java.text.SimpleDateFormat. "-MM-dd")
                           (new java.util.Date))))
      gen/nat)))
#_(s/conform ::birthdate "1999-22-11")
#_(s/conform ::birthdate (gen/generate date-generator))
#_(gen/generate (s/gen ::birthdate))

(s/def ::address string?)
;; (s/def ::ensuarance-id 
;;   (s/with-gen (s/and int? pos? #(= 16 (count (str %))))
;;     #(gen/large-integer* {:min 999999999999999})))
(s/def ::ensuarance-id string?)
#_(s/conform ::ensuarance-id 1999999999999999)

(s/def ::id 
  (s/with-gen (s/and int? pos?)
    #(gen/large-integer* {:min 8000000000000})))
#_(s/conform ::id 1602954077980)

(s/def ::patient 
  (s/keys :req-un [::name
                   ::gender
                   ::birthdate
                   ::address
                   ::ensuarance-id
                   ::id
                   ]))

;===============================
#_(gen/generate (s/gen ::patient))
#_(pp/pprint (s/conform ::patient (gen/generate (s/gen ::patient))))
#_(pp/pprint (s/unform ::patient
                       (s/conform ::patient (gen/generate (s/gen ::patient)))))
;===============================

;tables
; things: id, name, type
; params: id, name, thing_id,
(defn generate-patient []
  (gen/generate (s/gen ::patient)))

(defn validate-patient [p]
  (or (s/valid? ::patient p)
      (throw
       (ex-info "Invalid input"
                (s/explain-data ::patient p)))))

#_(s/conform ::name 2)
#_(s/explain-data ::name 1)
#_(s/explain ::name "")
#_(s/valid?  ::name 2)
