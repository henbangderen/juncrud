(ns ^:figwheel-hooks frontend.juncrud.core
  (:require
   [goog.dom :as gdom]
   [reagent.core :as r]
   [reagent.dom :as rdom]
   [cljs.core.async :as a]
   [ajax.core :as ajx]

   [reitit.frontend :as rf]
   [reitit.frontend.easy :as rfe]
   [reitit.coercion.spec :as rss]

   [clojure.string :as s]
   [fipp.edn :as fedn]
   ))

(defn ajax-call "Accept a cljs-ajax request map, and returns a channel which will contain the response,
or an Error if the response is an error."
  [{:keys [method uri] :as opts}]
  (let [=resp= (a/chan)]
    (ajx/ajax-request
     (assoc opts
            :handler
            (fn [[ok r :as data]]
              (if ok
                (a/put! =resp= r)
                (a/put! =resp= (ex-info "AJAX error" {:request opts :response r}))
                ))
            ))
    =resp=))

(def ajax-defaults "Basic options for the response format"
  {:format (ajx/json-request-format)
   :response-format (ajx/json-response-format {:keywords? true})})

;;STATE
(defonce app-state
  (r/atom {:text "Patients"
           :selected #{1234567890}
           :recently-added-patients []
           :recently-modified-patients []
           :total 0

           ;filter by search
           :search ""
           :filtered? false

           ;pagination
           :page-patients []
           :page-size 5
           :page-current 1
           :page-total 0
           }))

;;STATE MUTATION
(defn count-patients!
  ([]
   (let [chan
         (ajax-call
          (assoc ajax-defaults
                 :method :get :uri "count-patients"))
         out (a/chan)]
     (a/go (let [count (a/<! chan)]
             (swap! app-state assoc :total (js/parseInt count))
             (a/>! out count)
             (js/console.log "count-patients put to channel")
             ))
     out))
  ([search]
   (let [chan
         (ajax-call
          (assoc ajax-defaults
                 :method :get :uri "patients/search/count"
                 :params {:search search}))
         out (a/chan)]
     (a/go (let [count (a/<! chan)]
             (swap! app-state assoc :total (js/parseInt count))
             (a/>! out count)
             (js/console.log "count-patients put to channel")
             ))
     out))
  )

(defn offset[]
  (* (dec (:page-current @app-state))
     (:page-size @app-state)))

(defn fetch-patients-list!
  ([] (fetch-patients-list! (:page-size @app-state)
                            (offset)))
  ([limit offset]
   (let [chan
         (ajax-call
          (assoc ajax-defaults
                 :method :get
                 :uri "patients/fetch"
                 :params {:limit limit
                          :offset offset}))
         out (a/chan)]
     (a/go
       (let [patients (a/<! chan)]
         (swap! app-state assoc :page-patients patients)
         (a/>! out (count patients))
         (js/console.log "fetch-patients-list put to channel")))
     out))
  ([search]
   (let [chan
         (ajax-call
          (assoc ajax-defaults
                 :method :get
                 :uri "patients/search/fetch"
                 :params {:search search
                          :limit (:page-size @app-state)
                          :offset (offset)}))
         out (a/chan)]
     (a/go
       (let [patients (a/<! chan)]
         (swap! app-state assoc :page-patients patients)
         (a/>! out (count patients))
         (js/console.log "fetch-patients-list put to channel")))
     out))
  )

(defn delete-selected! [ids]
  (let [chan
        (ajax-call
         (assoc ajax-defaults
                :method :delete
                :uri "patients/delete"
                :params {:ids ids}))
        out (a/chan)]
    (a/go 
      (let [count (a/<! chan)]
        (a/>! out count)
        (js/console.log "delete-selected printed to channel: " count)))
    (swap! app-state assoc :selected #{})
    out))

(defn create-patient! [patient]
  (let [chan
        (ajax-call
         (assoc ajax-defaults
                :method :put
                :uri "patients/create"
                :params patient))
        out (a/chan)]
    (a/go (let [id (a/<! chan)]
            (a/>! out id)
            (js/console.log "patient created: " id "<<<<<<<")))
    out))

(defn modify-patient! [patient]
  (let [chan
        (ajax-call
         (assoc ajax-defaults
                :method :put
                :uri "patients/modify"
                :params patient))
        out (a/chan)]
    (a/go (let [id (a/<! chan)]
            (a/>! out id)
            (js/console.log "patient modified: " id "<<<<<<<")))
    out))

(defn init-app-state! []
  (when (empty? (:patients @app-state))
    (count-patients!)
    (fetch-patients-list!))
   (js/console.log "initialized"))
;;END STATE MUTATION  
;
(defn get-selected-patients []
  (filter (fn[item]
            (some #{(:id item)} (:selected @app-state)))
          (:page-patients @app-state)))

(defn remove-items [items]
  (let [filtered
        (filter (complement
                 (fn[item] (some #{(:id item)} items)))
                (:patients @app-state))]
    (swap! app-state assoc-in [:patients] filtered)))

(defn prepare-for-ajax[patient]
  (merge (select-keys patient [:id :gender :birthdate :address :ensuarance-id])
         {:name (clojure.string/join " " [(:last-name patient)
                                          (:first-name patient)
                                          (:second-name patient)])}))
(defn new-patient[m]
  (let [tmp-id 111
        new (merge (prepare-for-ajax m)
                   {:id tmp-id})]
    (swap! app-state update-in [:recently-added-patients] conj new)
    new))

(defn modify [m]
  (let [new (prepare-for-ajax m)]
    (swap! app-state update-in [:recently-modified-patients] conj new)
    new))

(def EVENT_CHANNEL (a/chan))
(def APP_EVENTS
  {
   :remove-items
   (fn [items]
     (a/go
       (a/<! (delete-selected! items))
       (a/<! (count-patients!))
       (a/<! (fetch-patients-list!))))

   :add-new-item
   (fn [new-patient-m]
     (let [new (new-patient new-patient-m)]
       (a/go 
         (a/<! (create-patient! new))
         (a/<! (count-patients!))
         (a/<! (fetch-patients-list!)))))
      
   :modify-item
   (fn [modified-patient-m]
     (modify-patient! 
      (modify modified-patient-m))
     (fetch-patients-list!))

   :next-page
   (fn []
     (when (< (:page-current @app-state)
              (:page-total @app-state))
       (swap! app-state assoc :selected #{})
       (swap! app-state update-in [:page-current] inc)
       (fetch-patients-list!)))

   :previous-page
   (fn []
     (when (>= (:page-current @app-state) 2)
       (swap! app-state assoc :selected #{})
       (swap! app-state update-in [:page-current] dec)
       (fetch-patients-list!)))

   :filter-by-search
   (fn [text-to-search]
     (count-patients! text-to-search)
     (fetch-patients-list! text-to-search))
   })

(init-app-state!)
(a/go (while true
       (let [[event-name event-data] (a/<! EVENT_CHANNEL)]
         ((event-name APP_EVENTS) event-data))))

(defn pagination[]
  (swap! app-state assoc-in [:page-total]
         (Math.ceil (/ (:total @app-state)
                       (:page-size @app-state))))
  (when (> (:page-current @app-state) (:page-total @app-state))
    (a/put! EVENT_CHANNEL [:previous-page]))

  (cond
    (= 0 (:page-total @app-state)) [:div]
    :else
    [:div {:class "pagination"}
     [:span (:page-current  @app-state) " / " (:page-total @app-state) " "]
     [:button {:class :back
               :on-click
               (fn[e] (a/put! EVENT_CHANNEL [:previous-page]))}
      "previous"]
     [:button {:class :forward
               :on-click
               (fn[e] (a/put! EVENT_CHANNEL [:next-page]))}
      "next"]]))

#_(reset! app-state {:text "!!Hello world!"
                     :patients
                     [{:name "Иванов Иван Иванович"
                       :gender "M"
                       :birthdate "1.09.1920"
                       :address "Тольятти Победы 42, 54"
                       :ensuarance-id 1234567890123456
                       :id 1234567890}
                      {:name "Иванов Иван Иванович"
                       :gender "M"
                       :birthdate "1.09.1920"
                       :address "Тольятти Победы 42, 54"
                       :ensuarance-id 1234567890123456
                       :id 1234567890}]})

(defn unzipmap [maps]
  (let [ks (keys (first maps))]
    [ks (map vals maps)]))

(defn wrap
  ([by xs] (map (fn[x] [by x]) xs))
  ([by by-options xs] (map (fn[x] [by by-options x]) xs)))

(defn toggle-selected-row [id]
  (if (some #{id} (:selected @app-state))
    (swap! app-state update-in [:selected] disj id)
    (swap! app-state update-in [:selected] conj id)))

(defn patient-row [row]
  (let [tds (wrap :td  (drop-last row))
        id (last row)]
    (into ^{:key id}
          [:tr
           {:id id
            :class (when (some #{id} (:selected @app-state))
                     "selected")
            :on-click
            #(toggle-selected-row id)}
           ]
          tds)))

(defn empty-table []
  [:table {:class "empty"} "There are no records yet"])

(defn table-component []
  (cond
    (empty? (:page-patients @app-state))
    (empty-table)

    :else
    (let [[headers rows] (unzipmap (:page-patients @app-state))]
      [:table#patients 
       [:thead [:tr (wrap :th (drop-last headers))]] ;drop id
       (into [:tbody ] (map patient-row rows))])))

(defn greet-number
  "I say hello to an integer"
  [num]                             ;; an integer
  [:div (str "Hello #" num)])       ;; [:div "Hello #1"]

(defn more-button
  "I'm a button labelled 'More' which increments counter when clicked"
  [counter]                                ;; a ratom
  [:div  {:class "button-class"
          :on-click  #(swap! counter inc)} ;; increment the int value in counter
   "More"])
(defn parent
  [] 
  (let [counter  (reagent.ratom/atom 1)]    ;; the render closes over this state
    (fn  parent-renderer 
      []
      [:div 
       [greet-number @counter]      ;; notice the @. The prop is an int
       [more-button counter]])))    ;


(defn add-button []
  [:button {:class "adder"
            :on-click
            (fn [e]
              (js/console.log "add button clicked")
;             (a/put! EVENT_CHANNEL [:add-item nil])
              (rfe/push-state ::add-patient)
              )}
   "Add"])

(defn remove-button []
  [:button {:class "remover"
            :on-click
            #(when (js/confirm "All selected patient records will be removed. Are you sure?")
               (a/put! EVENT_CHANNEL [:remove-items (:selected @app-state)]))}
   "Remove"])

(defn edit-button []
  [:button {:class "editor"
            :on-click
            (fn [e] (rfe/push-state ::edit-patient))}
   "Edit"])

(defn search-panel []
  [:div.search-panel
   [:input {:id :search :placeholder "Filter by search"
            :value (:search @app-state)
            :on-change
            (fn [e]
              (swap! app-state assoc :search (-> e .-target .-value)))}]
   [:button
    {:id :go
     :on-click
     (fn [e]
       (a/put! EVENT_CHANNEL [:filter-by-search (:search @app-state)])
       )}
    "Go"]])

(defn tool-panel []
  [:div.tool-panel
   [add-button]
   [edit-button]
   [remove-button]
   [search-panel]])

;(let [[a b] (s/split "The Quick Brown     Fox" #"\s+")] [a b])
(defn form-input[atom]
  [:<>
     [:fieldset
      [:legend "Person"]
      [:label {:for :last-name} "Last name"]
      [:input {:id :last-name :placeholder "Last name"
               :required "true"
               :value (:last-name @atom) ;strange deref @atom must be here, does not work another way
               :on-change (fn [e]
                            (swap! atom assoc-in [:last-name] (-> e .-target .-value))
                            )}][:br]
      
      [:label {:for :first-name} "First name"]
      [:input {:id :first-name :placeholder "First name"
               :required "true"
               :value (:first-name @atom)
               :on-change (fn [e]
                            (swap! atom assoc-in [:first-name] (-> e .-target .-value))
                            )}][:br]

      [:label {:for :sname} "Second name"]
      [:input {:id :sname :placeholder "Second name"
               :value (:second-name @atom)
               :on-change (fn [e]
                            (swap! atom assoc-in [:second-name] (-> e .-target .-value))
                            )}][:br]

      [:label {:for :gender} "Gender"]
      [:select {:id :gender
                :required "true"
                :value (:gender @atom)
                :on-change (fn [e]
                             (swap! atom assoc-in [:gender] (-> e .-target .-value)))}
       [:option "M"]
       [:option "F"]][:br]

      [:label {:for :birthdate} "Birthdate"]
      [:input {:id :birthdate :type :date
               :required "true"
               :value (:birthdate @atom)
               :on-change (fn [e]
                            (swap! atom assoc-in [:birthdate] (-> e .-target .-value)))}][:br]

      [:label {:for :address} "Address"]
      [:input {:id :address :placeholder "Address"
               :value (:address @atom)
               :on-change (fn [e]
                            (swap! atom assoc-in [:address] (-> e .-target .-value))
                            )}][:br]

      [:label {:for :ensuarance-id} "Ensuarance-Id"]
      [:input {:id :ensuarance-id :placeholder "Ensuarance Id"
               :required "true"
               :value (:ensuarance-id @atom)
               :on-change (fn [e]
                            (swap! atom assoc-in [:ensuarance-id] (-> e .-target .-value))
                            )}][:br]
      ]

     [:fieldset [:legend "Medical history"]
      [:textarea
       {:value (:medical-history @atom)
        :on-change (fn [e] (swap! atom assoc-in [:medical-history] (-> e .-target .-value)))}
       ]]
   ]
  )

(defn lister [patients]
  [:ul
   (for [p patients]
     ^{:key p} [:li (vals (select-keys p [:name ]))])])

(defn add-patient-page []
  (let [patient (r/atom {:first-name ""
                        :second-name ""
                        :last-name ""
                        :gender "M"
                        :birthdate "1955-04-01"
                        :address "Тольятти Победы 42, 54"
                        :ensuarance-id "123456"
                        :medical-history "It's alive"})
        ]
    (fn []
      [:div {:class "container"}
       [:h1 "Add a new patient"]
       [:form {:class ["addform"]
               :on-submit (fn [e]
                            (.preventDefault e)
                            (a/put! EVENT_CHANNEL [:add-new-item @patient]))}
        [form-input patient]
        [:input {:class "submitter" :type :submit :value "add"}]
        ]

       [:div {:class "recently-added"}
        [:p "Recently added"]
        [lister (:recently-added-patients @app-state)]]
       ]
      )))

(defn edit-patient-page []
  (let [patients (map (fn[x] (r/atom
                              (let [[last first second] (s/split (:name x) #"\s+")]
                                (merge x {:last-name last
                                          :first-name first
                                          :second-name second}))))
                      (get-selected-patients))]
    (fn []
      [:div {:class "container"}
       [:h1 "Edit info"]
       (for [patient-atom patients]
         [:form {:class ["editForm"]
                  :on-submit
                  (fn [e]
                    (.preventDefault e)
                    (a/put! EVENT_CHANNEL [:modify-item @patient-atom])
                    (. (-> e .-target) remove)
                    )}
           [form-input patient-atom]
          [:input {:type :submit :value "update"}]
          [:input {:type :button :value "cancel"
                   :on-click
                   (fn remove-parent-element [e]
                     (. (-> e .-target .-parentElement) remove))}]
          ])
       
       [:div {:class "recently-modified"}
        [:p "Recently modified"]
        [lister (:recently-modified-patients @app-state)]]])))


(defn patients-page []
    [:div
     [:h1 (:text @app-state)]
     [tool-panel]
     [table-component]
     [pagination]
     ])

(defn settings-page
  "Shows just the state currently"
  []
  [:div
   [:h1 "State"]
   [:pre (with-out-str (fedn/pprint @app-state))]
   ])

(defn home-page []
  [:div
   [:h2 "Welcome to frontend"]

   [:button
    {:type "button"
     :on-click #(rfe/push-state ::item {:id 3})}
    "Item 3"]

   [:button
    {:type "button"
     :on-click #(rfe/replace-state ::item {:id 4})}
    "Replace State Item 4"]])

(defn about-page []
  [:div
   [:h2 "About frontend"]
   [:ul
    [:li [:a {:href "http://google.com"} "external link"]]
    [:li [:a {:href (rfe/href ::foobar)} "Missing route"]]
    [:li [:a {:href (rfe/href ::item)} "Missing route params"]]]

   [:div
    {:content-editable true
     :suppressContentEditableWarning true}
    [:p "Link inside contentEditable element is ignored."]
    [:a {:href (rfe/href ::frontpage)} "Link"]]])

(defn item-page [match]
  (let [{:keys [path query]} (:parameters match)
        {:keys [id]} path]
    [:div
     [:h2 "Selected item " id]
     (if (:foo query)
       [:p "Optional foo query param: " (:foo query)])]))

(defonce match (r/atom nil))
     
(defn current-page []
  [:<>
   [:div
    [:h1 {:class :logo}]
    [:ul {:class :navigation-bar}
     [:li [:a {:href (rfe/href ::patients)} "Patients"]]
     [:li [:a {:href (rfe/href ::settings)} "State"]]
     [:li {:class "float-right"}
      [:a {:href (rfe/href ::frontpage)} "Frontpage"]]
     [:li {:class "float-right"}
      [:a {:href (rfe/href ::about)} "About"]]
     ]
    (if @match
      (let [view (:view (:data @match))]
        [view @match]))
       ;[:pre (with-out-str (fedn/pprint @match))]
    [:footer "© 2020 Roman Levakin. Today is better than tomorrow"]
    ]])

(def routes
  [
   ["/patients" ;in browser as /#/patients
    {:name ::patients
     :view patients-page}]
   ["/settings"
    {:name ::settings
     :view settings-page}]

   ["/add-patient"
    {:name ::add-patient
     :view add-patient-page}]

   ["/edit-patient"
    {:name ::edit-patient
     :view edit-patient-page}]
   
   ;; ["/"
   ;;  {:name ::frontpage
   ;;   :view home-page}]
    
   ;; ["/about"
   ;;  {:name ::about
   ;;   :view about-page}]

   ;; ["/item/:id"
   ;;  {:name ::item
   ;;   :view item-page
   ;;   :parameters {:path {:id int?}
   ;;   ;:query {(ds/opt :foo) keyword?}
   ;;                }}]
   ])

(defn init! []
  (rfe/start!
    (rf/router routes {:data {:coercion rss/coercion}})
    (fn [m] (reset! match m))
    ;; set to false to enable HistoryAPI
    {:use-fragment true})
  (rdom/render [current-page] (.getElementById js/document "app")))
(init!)


(defn mount-app-element []
  (when-let [el (gdom/getElement "app")]
    (rdom/render [patients-page] el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
;(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  ;(mount-app-element)
  (init!)
    ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
