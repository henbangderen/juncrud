(ns aus-figwheel-template.patient-spec
    (:require [clojure.spec.alpha :as s]
            [clojure.test.check.generators :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.pprint :as pp]))

(s/def ::name (s/cat :last-name string?
                     :first-name string?
                     :second-name string?))
#_(s/conform ::name ["Иванов" "Иван" "Иванович"])

(s/def ::gender #{"M" "F"})
(s/def ::birthdate (s/cat :day string? :month string? :year string?))
(s/def ::address (s/cat :city-town-village string?
                        :street-name-number-room string?))
(s/def ::ensuarance-id 
  (s/with-gen (s/and int? pos? #(= 16 (count (str %))))
    #(gen/large-integer* {:min 999999999999999})))

(s/def ::id 
  (s/with-gen (s/and int? pos?)
    #(gen/large-integer* {:min 80000000000000})))

(s/def ::patient 
  (s/keys :req-un [::name
                   ::gender
                   ::birthdate
                   ::address
                   ::ensuarance-id
                   ::id
                   ]))

(s/def ::patient
  (s/keys :req-un [::name
                   ::gender
                   ::birthdate
                   ::address
                   ::ensuarance-id
                   ::id
                   ]))

;===============================
(s/def :db/name string?)
(s/def ::type #{:patient})

(s/def :db/thing
  (s/keys :req-un [:db/name
                   ::type
                   ::id]))

(s/def :db/thing-id ::id)
(s/def :db/param
  (s/keys :req-un [:db/name :db/thing-id]))
(gen/generate (s/gen :db/param))

(defn params-of-same-thing? [params]
  (and (vector? params)
       (apply = (filter (fn [p] (:thing-id p))
                  params))))
(params-of-same-thing? [
                          {:id 80000000000401, :name "Rk6G5HSZK5r9Q86GLt9XWYB", :thing-id 80000196929590}
                          {:id 80000000000071, :name "N6aCXTRZ2", :thing-id 80000003523691}
                          {:id 80000000000321, :name "KB9FwQ6p7tMx03Ii7OT", :thing-id 80000000043076}])

(s/def :db/params
  (s/coll-of :db/param :kind vector? :max-count 5 :distinct true))



#_(gen/generate (s/gen :db/params))
#_(pp/pprint (s/conform ::patient (gen/generate (s/gen ::patient))))
#_(pp/pprint (s/unform ::patient
                       (s/conform ::patient (gen/generate (s/gen ::patient)))))
;===============================
(s/unform ::patient 
          {:name {:last-name "Иванов"
                  :first-name "Иван"
                  :second-name "Иванович"}
           :gender "M"
           :birthdate {:day "1" :month "09" :year "1920"}
           :address {:city-town-village "Тольятти"
                     :street-name-number-room "Победы 42, 54"}
           :ensuarance-id 1000000000000011
           :id 100000000000001820})

(s/unform ::patient 
          {:name {:last-name "lSnCjJ7XjfOxOeszhN1gAq5",
                  :first-name "hN9",
                  :second-name "Zl5GNqMZ0W7hhN4M6"},
           :gender "M",
           :birthdate {:day "uTk7k14YDs8", :month "1bMq2", :year ""},
           :address {:city-town-village "WI0Hr8", :street-name-number-room "j0T9806x80e"},
           :ensuarance-id 1000000000000011,
           :id 100000000000001820})

(defn new-patient [patient]
  (let [unform (s/unform ::patient patient)]
    unform))

(s/explain ::patient
          (new-patient {:name {:last-name "lSnCjJ7XjfOxOeszhN1gAq5",
                               :first-name "hN9",
                               :second-name "Zl5GNqMZ0W7hhN4M6"},
                        :gender "M",
                        :birthdate {:day "uTk7k14YDs8", :month "1bMq2", :year ""},
                        :address {:city-town-village "WI0Hr8", :street-name-number-room "j0T9806x80e"},
                        :ensuarance-id 1000000000000011,
                        :id 100000000000001820}))


;tables
; objects: id, name, type
; params: id, objectId, name,
{:name {:last-name "Иванов"
                  :first-name "Иван"
                  :second-name "Иванович"}
           :gender "M"
           :birthdate "1.09.1920"
           :address {:city-town-village "Тольятти"
                     :street-name-number-room "Победы 42, 54"}
           :ensuarance-id "1313 1232 3234 2342"
           :id "1312 2342 3243 2332 2343"}
