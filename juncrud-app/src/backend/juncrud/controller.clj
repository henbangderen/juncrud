(ns backend.juncrud.controller
  (:require [taoensso.timbre :as timbre :refer [info debug error]]
            [backend.juncrud.crud :as crud]
            [common.juncrud.patient-spec :as p]
            [clojure.data.json :as json]))

(defn fetch-patients
  ([page-size offset]
   (json/write-str (crud/fetch-patients page-size offset)))
  ([search page-size offset]
   (json/write-str (crud/fetch-patients search page-size offset))))

(defn count-patients
  ([] (crud/count-patients))
  ([search] (crud/count-patients search)))

(defn delete-patients [ids] (crud/delete-patients! ids))

(defn create-patient [patient] (crud/create-patient! patient))

(defn edit-patient [patient] (crud/update-patient! patient))
