(ns backend.juncrud.logconfig
  (:require
   [taoensso.timbre :as timbre :refer [info debug error]]
   [taoensso.timbre.appenders.core :as appenders]
   [clojure.string :as str]
   [outpace.config :refer [defconfig]]))

(defconfig log-dir "target")
(defconfig log-file-name "server.log")

(def timestamp-opts
  "Controls (:timestamp_ data)"
  {:pattern  "yyyyMMdd:HHmmss"
   :locale   :jvm-default #_(java.util.Locale. "en")
   :timezone :utc         #_(java.util.TimeZone/getTimeZone "Europe/Amsterdam")})
(defn output-fn
  ([     data] (output-fn nil data))
  ([opts data] ; For partials
   (let [{:keys [no-stacktrace? stacktrace-fonts]} opts
         {:keys [level ?err #_vargs msg_ ?ns-str ?file
                 timestamp_ ?line]} data]
     (str (force timestamp_)       " "
          (str/upper-case (name level))  " "
          "[" (or ?ns-str ?file "?") ":" (or ?line "?") "]- "
          (force msg_)
          (when-not no-stacktrace?
            (when-let [err ?err]
              (str "\n" (timbre/stacktrace err opts))))))))

(timbre/merge-config!
 {:timestamp-opts timestamp-opts
  :output-fn output-fn
  :appenders {:spit (merge {:hostname_ ""}
                           (appenders/spit-appender {:fname (str log-dir "/" log-file-name)}))}})

