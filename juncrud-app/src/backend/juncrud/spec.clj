(ns juncrud.spec
  (:require [clojure.spec.alpha :as s]
            [clojure.test.check.generators :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.pprint :as pp]))

(s/def ::big-even (s/and int? even? #(> % 1000)))
(s/valid? ::big-even 42)
(s/valid? ::big-even 4200)

(s/def ::id (s/and pos-int?))
(s/def ::email (s/and string? (partial re-matches #"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$")))

;validates
(s/valid? ::id 1)
;=> true
(s/valid? ::id 0)
;=> false

;conforms
(s/def ::name-or-id (s/or :name string?
                          :id ::id))
(s/conform ::name-or-id 1)
;=> [:id 1]
(s/conform ::name-or-id "foo")
;=> [:name "foo"]
(s/conform ::name-or-id 0)
;=> :clojure.spec.alpha/invalid

;generate data
;(gen/sample (s/gen ::id))
;=> (2 1 1 3 1 6 1 2 8 2)
;(gen/generate (s/gen ::id))
;=> 86


;constraint
(s/def ::email (s/and string? (partial re-matches #"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$")))

(s/def ::password-not-empty not-empty)
(s/def ::password-length #(<= 6 (count %)))
(s/def ::password (s/and string? ::password-not-empty ::password-length))

(s/def ::form (s/keys :req-un [::email ::password]))

(s/explain-data ::form {:email "foo@example.com"
                        :password "qwas"})

;--------------------------------------------------------------
(s/def ::email (s/and string? (partial re-matches #"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$")))

(s/def ::password-not-empty not-empty)
(s/def ::password-length #(<= 6 (count %)))
(s/def ::password (s/and string? ::password-not-empty ::password-length))

(s/def ::form (s/keys :req-un [::email ::password]))


(gen/sample (s/gen (s/cat :k keyword? :ns (s/+ number?))))
(s/exercise (s/cat :k keyword? :ns (s/+ number?)) 5)
;;=>
;;=> ((:D -2.0)


                                       ;tutorial
(s/def :unq/person
  (s/keys :req-un [::first-name ::last-name ::email]
          :opt-un [::phone]))

(s/conform :unq/person
  {:first-name "Bugs"
   :last-name "Bunny"
   :email "bugs@example.com"})
;;=> {:first-name "Bugs", :last-name "Bunny", :email "bugs@example.com"}

(gen/generate (s/gen :unq/person))

(s/explain :unq/person
  {:first-name "Bugs"
   :last-name "Bunny"
   :email "n/a"})
;; "n/a" - failed: (re-matches email-regex %) in: [:email] at: [:email]
;;   spec: :my.domain/email-type

(s/explain :unq/person
  {:first-name "Bugs"})
;; {:first-name "Bugs"} - failed: (contains? % :last-name) spec: :unq/person
;; {:first-name "Bugs"} - failed: (contains? % :email) spec: :unq/person


;multi-spec
(s/def :event/type keyword?)
(s/def :event/timestamp int?)
(s/def :search/url string?)
(s/def :error/message string?)
(s/def :error/code int?)

(defmulti event-type :event/type)
(defmethod event-type :event/search [_]
  (s/keys :req [:event/type :event/timestamp :search/url]))
(defmethod event-type :event/error [_]
  (s/keys :req [:event/type :event/timestamp :error/message :error/code]))

(s/def :event/event (s/multi-spec event-type :event/type))

(s/valid? :event/event
  {:event/type :event/search
   :event/timestamp 1463970123000
   :search/url "https://clojure.org"})
;=> true
(s/valid? :event/event
  {:event/type :event/error
   :event/timestamp 1463970123000
   :error/message "Invalid host"
   :error/code 500})
;=> true
(s/explain :event/event
  {:event/type :event/restart})
;; #:event{:type :event/restart} - failed: no method at: [:event/restart]
;;   spec: :event/event
(s/explain :event/event
  {:event/type :event/search
   :search/url 200})
;; 200 - failed: string? in: [:search/url]
;;   at: [:event/search :search/url] spec: :search/url
;; {:event/type :event/search, :search/url 200} - failed: (contains? % :event/timestamp)
;;   at: [:event/search] spec: :event/event


(s/conform (s/coll-of keyword?) [:a :b :c])
(s/def ::scores (s/map-of string? int?))
(s/conform ::scores {"Sally" 1000, "Joe" 500})
;=> {"Sally" 1000, "Joe" 500}

(s/def ::ingredient (s/cat :quantity number? :unit keyword?))
(s/conform ::ingredient [2 :teaspoon])


(s/def ::nested
  (s/cat :names-kw #{:names}
         :names (s/spec (s/* string?))
         :nums-kw #{:nums}
         :nums (s/spec (s/* number?))))
(s/conform ::nested [:names ["a" "b"] :nums [1 2 3]])
;;=> {:names-kw :names, :names ["a" "b"], :nums-kw :nums, :nums [1 2 3]}

(s/def ::unnested
  (s/cat :names-kw #{:names}
         :names (s/* string?)
         :nums-kw #{:nums}
         :nums (s/* number?)))
(s/conform ::unnested [:names "a" "b" :nums 1 2 3])
;;=> {:names-kw :names, :names ["a" "b"], :nums-kw :nums, :nums [1 2 3]}

(s/def ::config (s/*
                  (s/cat :prop string?
                         :val  (s/alt :s string? :b boolean?))))
(s/conform ::config ["-server" "foo" "-verbose" true "-user" "joe"])
;;=> [{:prop "-server", :val [:s "foo"]}
;;    {:prop "-verbose", :val [:b true]}
;;    {:prop "-user", :val [:s "joe"]}]

(defn- set-config [prop val]
  ;; dummy fn
  (println "set" prop val))

(defn configure [input]
  (let [parsed (s/conform ::config input)]
    (if (= parsed ::s/invalid)
      (throw (ex-info "Invalid input" (s/explain-data ::config input)))
      (for [{prop :prop [_ val] :val} parsed]
        (set-config (subs prop 1) val)))))

(configure ["-server" "foo" "-verbose" true "-user" "joe"])

;spec'ing functions
(defn ranged-rand
  "Returns random int in range start <= rand < end"
  [start end]
  (+ start (long (rand (- end start)))))

(defn ranged-rand  ;; BROKEN!
  "Returns random int in range start <= rand < end"
  [start end]
  (+ start (long (rand (- start end)))))

(s/fdef ranged-rand
  :args (s/and (s/cat :start int? :end int?)
               #(< (:start %) (:end %)))
  :ret int?
  :fn (s/and #(>= (:ret %) (-> % :args :start))
             #(< (:ret %) (-> % :args :end))))
(stest/check `ranged-rand) ; awesome!
(stest/abbrev-result (first (stest/check `ranged-rand)))

(defn adder [x] #(+ x %))
(s/fdef adder
  :args (s/cat :x number?)
  :ret (s/fspec :args (s/cat :y number?)
                :ret number?)
  :fn #(= (-> % :args :x) ((:ret %) 0)))

;end tutorial


;;
;; PATIENT SPEC
;;
(s/def ::name (s/cat :last-name string?
                     :first-name string?
                     :second-name string?))
;(s/conform ::name ["Иванов" "Иван" "Иванович"])

(s/def ::gender #{"M" "F"})
(s/def ::birthdate (s/cat :day string? :month string? :year string?))
(s/def ::address (s/cat :city-town-village string?
                        :street-name-number-room string?))
(s/def ::ensuarance-id 
  (s/with-gen (s/and int? pos? #(= 16 (count (str %))))
    #(gen/large-integer* {:min 999999999999999})))

(s/def ::id 
  (s/with-gen (s/and int? pos? #(= 18 (count (str %))))
    #(gen/large-integer* {:min 99999999999999999})))

(s/def ::patient 
  (s/keys :req-un [::name
                   ::gender
                   ::birthdate
                   ::address
                   ::ensuarance-id
                   ::id
                   ]))
(pp/pprint (s/conform ::patient (gen/generate (s/gen ::patient))))

{:name {:last-name "Иванов"
        :first-name "Иван"
        :second-name "Иванович"}
 :gender "M"
 :birthdate "1.09.1920"
 :address {:city-town-village "Тольятти"
           :street-name-number-room "Победы 42, 54"}
 :ensuarance-id "1313 1232 3234 2342"
 :id "1312 2342 3243 2332 2343"}


