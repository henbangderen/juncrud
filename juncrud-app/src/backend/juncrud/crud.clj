(ns backend.juncrud.crud
  "works with the tables:
  THINGS: id, name, type
  PARAMS: id, objectId, name"
  (:require [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [next.jdbc.result-set :as result-set]
            [clojure.string :as s]
            [outpace.config :refer [defconfig]]
            [clojure.java.io :as io]
            [common.juncrud.patient-spec :as spec]
            [backend.juncrud.crud-util :as util]
            [taoensso.timbre :as timbre :refer [info debug error]]))

(defconfig dbname "jundb")
(defconfig host "localhost")
(defconfig port "5432")
(defconfig user "jun")
(defconfig password "jun")
(def db {:dbtype "postgresql" 
         :dbname dbname
         :host host
         :port port
         :user user
         :password password})
(info "started with configuration: " (dissoc db :password))
(defn get-datasource [db]
  (jdbc/with-options (jdbc/get-datasource db)
    {:builder-fn result-set/as-unqualified-lower-maps}))
(def ds (get-datasource db))

(defn set-db! [new-db]
  (def db new-db)
  (def ds (get-datasource db)))

(defn getid! "returns a long number like 1601671859756355" []
  (:getid (jdbc/execute-one! ds ["select getId()"])))

(defn unzipmap [maps]
  (let [ks (keys (first maps))]
    [ks (map vals maps)]))

;todo in transaction
(defn create-patient! [p]
  (let [entity (util/patient->entity p getid!)]
    (sql/insert! ds :things (:things entity))

    (let [[columns & values] (:params entity)]
      (sql/insert-multi! ds :params
                         columns
                         values))
    
    (get-in entity [:things :id])))
;(create-patient! (spec/generate-patient))

(defn read-patient [id]
  (let [things 
        (sql/get-by-id ds :things id)

        params
        (sql/find-by-keys ds :params
                          {:thing_id id}
                          {:order-by [[:id :asc]]
                           :builder-fn result-set/as-unqualified-arrays
                           })]
    (util/entity->patient {:things things 
                           :params params}
                          )))
;(read-patient 1602975869766)

(defn update-patient! [patient]
  (let [id (:id patient)]
    (jdbc/with-transaction [tx ds]
      (sql/update! tx :things {:name (:name patient)} {:id id :type "patient"})
      
      (sql/update! tx :params {:value (:gender patient)} {:name (name :gender) :thing_id id})
      (sql/update! tx :params {:value (:birthdate patient)} {:name (name :birthdate) :thing_id id})
      (sql/update! tx :params {:value (:address patient)} {:name (name :address) :thing_id id})
      (sql/update! tx :params {:value (:ensuarance-id patient)} {:name (name :ensuarance-id) :thing_id id})
      ;; (for [[k v] (select-keys patient [:gender :birthdate :address :ensuarance-id])]
      ;;   (sql/update! ds :params
      ;;                {:value v}
      ;;                {:name (name k) :thing_id id}))
      id)))
#_(update-patient! {:name "Tom Smith Jn",
                  :gender "M",
                  :birthdate "1948-10-21",
                  :address "Briton Sure",
                  :ensuarance-id "21212414",
                  :id 1603273446312})
#_(sql/find-by-keys ds :params { :thing_id 1603273446312} {}) 

(defn delete-patient! [id]
  (sql/delete! ds :things {:id id}))
#_(delete-patient! 1603368543596) 
(defn delete-patients! [ids]
  (reduce + 0 (map (fn [id] (-> (delete-patient! id)
                                first ;parse it [:next.jdbc/update-count 1]
                                second))
                   ids)))

;(sql/insert! ds :address {:name "A. Person" :email "albert@person.org"})
;; equivalent to
;; (jdbc/execute-one! ds ["INSERT INTO address (name,email) VALUES (?,?)" 
;;                        "A.Person" "albert@person.org"] {:return-keys true})

;; (sql/insert-multi! ds :address
;;   [:name :email]
;;   [["Stella" "stella@artois.beer"]
;;    ["Waldo" "waldo@lagunitas.beer"]
;;    ["Aunt Sally" "sour@lagunitas.beer"]])
;; ;; equivalent to
;; (jdbc/execute! ds ["INSERT INTO address (name,email) VALUES (?,?), (?,?), (?,?)"
;;                    "Stella" "stella@artois.beer"
;;                    "Waldo" "waldo@lagunitas.beer"
;;                    "Aunt Sally" "sour@lagunitas.beer"] {:return-keys true})
(defn- filter-by-search-query [search]
  (util/replace-templates
   (slurp (io/resource "query/filter-by-search.tpl"))
   {:search search}))

(defn fetch-patients
  ([limit] (fetch-patients limit 0))
  ([limit offset]
   (let [rs
         (jdbc/execute! ds
                       ["SELECT id FROM things 
                         WHERE type = 'patient' 
                         ORDER BY name ASC
                         LIMIT ?
                         OFFSET ?" limit offset])
         ids (map #(:id %) rs)]
     (map read-patient ids)))
  ([search limit offset]
   (let [rs (jdbc/execute!
             ds
             [(str (filter-by-search-query search)
                   "ORDER BY id ASC
                    LIMIT ?
                    OFFSET ?")
              limit offset])
         ids (map #(:id %) rs)]
     (map read-patient ids)))
  )

#_(fetch-patients 10)
#_(sql/find-by-keys ds :things {:type "patient"} {:order-by [[:name :asc]]})

(defn count-patients
  ([]
   (-> (jdbc/execute-one! ds ["select count(1) from things where type = 'patient'"])
       :count))
  ([search]
   (-> (jdbc/execute-one!
        ds
        [(str "select count(1) from ("
              (filter-by-search-query search)
              ") as TEMP")])
       :count)))


