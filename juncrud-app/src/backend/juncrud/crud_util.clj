(ns backend.juncrud.crud-util
  (:require [common.juncrud.patient-spec :as spec]))

;; It does not matter what id comes from UI
;; Correct ID is generated on DB
(defn patient->entity [patient getid-fn]
  {:pre [(spec/validate-patient patient)]}
  (let [id (getid-fn)]
    {:things {:id id
              :name (:name patient)
              :type "patient"}

     :params [[:id :name :value :thing_id]
              [(getid-fn) (name :gender) (:gender patient) id]
              [(getid-fn) (name :birthdate) (:birthdate patient) id]
              [(getid-fn) (name :address) (:address patient) id]
              [(getid-fn) (name :ensuarance-id) (:ensuarance-id patient) id]]
     }))

(defn entity->patient [entity]
  (let [[columns & rows] (:params entity)
        xs
        (map (fn [row] (zipmap columns row))
             rows)
        params
        (reduce (fn [accum x]
                  (assoc accum (keyword (:name x))
                         (:value x)))
                {}
                xs)]
    (merge {:name (get-in entity [:things :name])}
           params
           {:id (get-in entity [:things :id])}))
  )

(defn replace-templates
  "Returns a string with each occurrence of the form `{key}` in a
  `template` replaced with the corresponding value from a map
  `m`. Keys of `m` are expected to be keywords."
  [template m]
  (clojure.string/replace template #"\{([^{]+?)\}"
                          (fn [[orig key]] (or (get m (keyword key)) orig))))
