(ns backend.juncrud.server
  (:gen-class)
  (:require [backend.juncrud.config]
            [outpace.config :refer [defconfig]]
            [org.httpkit.server :refer [run-server]]
            [ring.middleware.resource :as resource]
            [ring.util.response :as response]
            [ring.middleware.defaults :as rmd]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [taoensso.timbre :as timbre :refer [info debug error]]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [backend.juncrud.controller :as controller]
            ))

(defconfig port "9090")

(defn server-log [handler]
  (fn [request]
    (info request)
    (let [response (handler request)]
      (info "  " response "\n\n")
      response)))

(defn create-patient [req]
  (info "create patient")
  "<h1>Good!</h1>")

;todo test it
(defn parse-json [request]
  (let [s (with-open [rdr (io/reader (:body request))]
            (s/join "" (line-seq rdr)))]
    (json/read-str s :key-fn keyword)))

(defroutes handler
  (GET "/" [] "<h1>Hello World!</h1>")
  (GET "/count-patients" [] (str (controller/count-patients)))
  (context
   "/patients" []
   (PUT "/create" req (str (controller/create-patient (parse-json req))))
   (PUT "/modify" req (str (controller/edit-patient (parse-json req))))
   (GET "/fetch" [limit offset] 
        (controller/fetch-patients (Integer/parseInt limit)
                                   (Integer/parseInt offset)))
   (context
    "/search" []
    (GET "/count" [search]
         (str (controller/count-patients search)))
    (GET "/fetch" [search limit offset] 
         (controller/fetch-patients search
                                    (Integer/parseInt limit)
                                    (Integer/parseInt offset))))
   (GET "/" [] (slurp (io/resource "public/patients.html")))
   (DELETE "/delete" req
           (let [s 
                 (with-open [rdr (io/reader (:body req))]
                   (s/join "" (line-seq rdr)))
                 j (json/read-str s :key-fn keyword)]
             (str (controller/delete-patients (:ids j))))))
  (GET "/create" [] create-patient)
  (POST "/create" [] create-patient)
  (route/not-found "<h1>Page not found!</h1>"))


(def app
  (resource/wrap-resource (rmd/wrap-defaults
                           (server-log handler)
                           (assoc-in rmd/site-defaults
                                     [:security :anti-forgery] false))
                          "public"))

(defonce server (atom nil))

(defn stop-server []
  (when-not (nil? @server)
    ;; graceful shutdown: wait 100ms for existing requests to be finished
    ;; :timeout is optional, when no timeout, stop immediately
    (@server :timeout 100)
    (reset! server nil)))

(defn start-server[]
  (reset! server (run-server app {:port (Integer/parseInt port)}))
  (println "server started on the port: " port))

(defn -main [& args]
  ;; The #' is useful when you want to hot-reload code
  ;; You may want to take a look: https://github.com/clojure/tools.namespace
  ;; and http://http-kit.org/migration.html#reload
  (start-server))

;;CURL
;; curl -X DELETE http://localhost:9090/patients/delete -d '{"delete":{"query":"field:value"}}' -H 'Content-Type: application/json'
;; {"delete":{"query":"field:value"}}
;;END CURL
