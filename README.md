#  juncrud

Sample clojurescript (reagent) + postgres CRUD app. View, add, remove and edit a patient info.

##  [juncrud-app](./juncrud-app/README.md#juncrud-app)    
##  [juncrud-acceptance-tester](./juncrud-acceptance-tester/README.md#juncrud acceptance tester)



### Demo:
create<br/>  
![](juncrud-screenshots/create.gif)

update<br/>  
![](juncrud-screenshots/update.gif)
	
remove<br/>  
![](juncrud-screenshots/remove.gif)

filter-by-search<br/>  
![](juncrud-screenshots/filter-by-search.gif)

	
